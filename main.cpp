#include <iostream>
#include <map>
#include <regex>
#include <sstream>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

/*Crawls the specified webpage for html links, then recursively
follows each of those links and does the same */
void crawl(std::string url, int maxDepth, int currentDepth, std::map<std::string, int>& visitedSites);

//Extracts html links from the specified webpage using regular expressions
void extractLinks(std::string htmlLink, std::string url, int maxDepth, int currentDepth, std::map<std::string, int>& visitedSites);

//Adds key-value pair to std::map
void addUrl(std::string key, std::map<std::string, int>& visitedSites);

bool hasBeenVisited(std::map<std::string, int> visitedSites, std::string site);

void printSite(std::string site, int depth);

int main(int argc, char** argv) {
    std::string url = "cs.usu.edu";
    int maxDepth = 3;
    int initialDepth = 1;
    std::map<std::string, int> visitedSites;

    if (argc > 1)
        url = argv[1];

    if (argc > 2)
        maxDepth = atoi(argv[2]);
        
    std::cout << "Recursively crawling the web to a depth of " << maxDepth << " links beginning from " << url << std::endl;
    
    addUrl(url, visitedSites);
    crawl(url, maxDepth, initialDepth, visitedSites);
    return 0;
}

/* 1. Retrieves specified url
   2. Hands url html to function 'extractLinks' which parses html for links and displays them
*/
void crawl(std::string url, int maxDepth, int currentDepth, std::map<std::string, int>& visitedSites) {
    if (currentDepth <= maxDepth) {
        try {
            //Code adapted from curlppExample.cpp by Jean-Philippe Barrette-Lapierre
            curlpp::Cleanup cleaner;
            
            curlpp::Easy urlRequest;
            
            curlpp::options::Url curlUrl(url);
            
            urlRequest.setOpt(curlUrl);
            
            //Allows urlRequest to follow redirects
            curlpp::options::FollowLocation follow(true);
            urlRequest.setOpt(follow);
            
            std::ostringstream os;
            //Passing webpage info to the stream
            os << urlRequest << std::endl;
            
            //Converting the webpage info into a string
            std::string htmlContents(os.str());
            
            //Extracts links from url's html and prints them to console. Recursively crawls unvisited links
            extractLinks(htmlContents, url, maxDepth, currentDepth, visitedSites);
        }
        catch (curlpp::LogicError & e) {
            std::cout << e.what() << std::endl;
        }
        catch (curlpp::RuntimeError & e) {
            std::cout << e.what() << std::endl;
        }
    }
}

void extractLinks(std::string htmlContents, std::string url, int maxDepth, int currentDepth, std::map<std::string, int>& visitedSites) {
    //Regex pattern to match href hyperlinks
    std::regex pattern("<\\s*a.*href=['\"](https?:[/]{2}[^/\"]+)?([^ '\"#]+)?");
    std::smatch match;
    
    while (std::regex_search(htmlContents, match, pattern)) {
        //Prints same-domain url
        if (match[1] == "") {
            std::string site = url + match[2].str();
            addUrl(site, visitedSites);

            if (!hasBeenVisited(visitedSites, site)) {
                printSite(site, currentDepth);
                
                int nextDepth = currentDepth + 1;
                crawl(site, maxDepth, nextDepth, visitedSites);
            }
        }
        
        //Prints different site url
        else {
            std::string site = match[1].str() + match[2].str();
            addUrl(site, visitedSites);

            if (!hasBeenVisited(visitedSites, site)) {
                printSite(site, currentDepth);
                
                int nextDepth = currentDepth + 1;
                crawl(site, maxDepth, nextDepth, visitedSites);
            }
        }
        
        //Resets htmlContents to begin where last search ended
        htmlContents = match.suffix().str();
    }
}

void addUrl(std::string key, std::map<std::string, int>& visitedSites) {
    if (visitedSites.count(key) == 0) {
        visitedSites[key] = 0;
    }
    
    visitedSites[key]++;
}

bool hasBeenVisited(std::map<std::string, int> visitedSites, std::string site) {
    if (visitedSites[site] <= 1) {
        return false;
    }
    else 
        return true;
}

void printSite(std::string site, int depth) {
    for (int i = 1; i < depth; i++) {
        std::cout << "\t";
    }
    std::cout << site << std::endl;
}