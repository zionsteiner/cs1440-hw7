// http://www.cplusplus.com/reference/regex/regex_search/

/*
 * This program demonstrates how the C++ regular expression library works.
 *
 * It searches for the substring "sub" within a large string that contains many
 * occurrences. This is similar to what you'll do to search for hyperlinks
 * within an HTML document.
 */

#include <iostream>
#include <string>
#include <regex>

int main () {
    // TODO: put some HTML <a> tags in here for testing with the hrefRegexSource regex
    std::string haystack("<a href=\"/articles/index.aspx\">Articles</a>\n"
    "<a class=\"arrow\" href=\"/promotions/informits-community-resource-center-139745\">Community</a>\n"
    "<a href=\"/store/index.aspx?st=60356\">Home &amp; Office Computing</a>\n"
"<a href='https://www.safaribooksonline.com/?utm_source=informit&amp;utm_medium=referral&amp;utm_campaign=publisher&amp;utm_content=global+top+nav'>Safari</a>\n"
"<a id=\"primary\" href='://googoo.com/articles/index.aspx'>Articles index</a>\n"
"<a href=\"googoo.com/articles/index.aspx\" id=\"secondary\" class=\"non-important\">Secondary shopping choice</a>\n");

    // Here is the regex we built in class. It may not be completely correct, but it should be
    // good enough for your assignment. Do feel free to improve it, if you wish :D
    //
    std::string hrefRegexSource("<\\s*a.*href=['\"](https?:[/]{2}[^/]+)?([^ '\"#]+)");
    
    // Here's an alternative regex with 5 capture groups which you may have better luck using:
    
    // https://bitbucket.org/snippets/hhenrichsen/xeerEd
    //regex href_re("<a .*?href=\"(((https?://)([^/]+)?|/)([^\"#]*))\".*?>"); // Hunter's regex

    std::string regexSourceCode("\\b(sub)([^ \n\t]*)");   // matches words beginning by "sub"
    std::regex needle(hrefRegexSource);
    std::smatch match;

    std::cout << "Regex source code (the needle):" << regexSourceCode  << std::endl;
    std::cout << "Target sequence (the haystack): " << haystack << std::endl << std::endl;
    std::cout << "The following matches and submatches were found:" << std::endl;

    while (std::regex_search(haystack, match, needle)) {
        int i = 0;
        for (auto submatch : match) {
            // This line of code illustrates that you can use the match object as though it's an 
            // array or a map through the subscript operator []
            std::cout << "\t[" << i << "] " << match[i] << " ";
            i++;
        }
        std::cout << std::endl;
        haystack = match.suffix().str();
       
    }
    std::cout << std::endl;
}
